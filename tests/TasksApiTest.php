<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class TasksApiTest extends TestCase
{
    use DatabaseTransactions;

    public function testTaskCreate()
    {
        $creator = factory(App\Models\User::class)->create();
        $project = factory(App\Models\Project::class)->create(['owner_id' => $creator->id]);
        $task = factory(App\Models\Task::class)->make([
            'creator_id' => $creator->id,
            'project_id' => $project->id,
        ])->toArray();
        $result = $task;
        unset($result['creator_id']);
        unset($result['project_id']);
        $this->post('/api/tasks', $task)->seeJson($result);
    }

    public function testTaskUpdate()
    {
        $creator = factory(App\Models\User::class)->create();
        $project = factory(App\Models\Project::class)->create(['owner_id' => $creator->id]);
        $task = factory(App\Models\Task::class)->create([
            'creator_id' => $creator->id,
            'project_id' => $project->id,
        ]);
        $newTask = factory(App\Models\Task::class)->make([
            'creator_id' => $creator->id,
            'project_id' => $project->id,
            'title'      => 'new title',
            'content'    => 'new content',
        ])->toArray();
        $this->put('/api/tasks/' . $task->id, $newTask)
                ->seeInDatabase('task', ['id' => $task->id, 'title' => 'new title', 'content' => 'new content']);
    }

    public function testTaskDestroy()
    {
        $creator = factory(App\Models\User::class)->create();
        $project = factory(App\Models\Project::class)->create(['owner_id' => $creator->id]);
        $task = factory(App\Models\Task::class)->create([
            'creator_id' => $creator->id,
            'project_id' => $project->id,
        ]);
        $resp = $this->call('DELETE', '/api/tasks/' . $task->id);
        $this->assertEquals(200, $resp->status());
        $respNotExists = $this->call('DELETE', '/api/tasks/99999999999999');
        $this->assertEquals(404, $respNotExists->status());
    }

    public function testTaskList()
    {
        $this->get('/api/tasks')->seeJson([]);
    }
}
