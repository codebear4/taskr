import '../css/app.less'
import 'animate.css'
import 'vue-animate-css'
import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import auth from './auth.js'
import App from './component/App.vue'
import Tasks from './component/Tasks.vue'
import Signin from './component/Signin.vue'
import Signup from './component/Signup.vue'

Vue.use(VueResource)
Vue.use(VueRouter)

let router = new VueRouter()

router.map({
  '/home': {
    component: Tasks
  },
  '/signin': {
    component: Signin
  },
  '/signup': {
    component: Signup
  },
})

router.redirect({
  '*': '/home'
})

Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token')

auth.checkAuth()

Vue.config.debug = true

export default router

router.start(App, '#app')
