import crypto from 'crypto'

export function gravatar_hash(email) {
    email = email.trim().toLowerCase()
    return crypto.createHash('md5').update(email).digest('hex')
}
export function gravatar_url(email, size) {
      return '//gravatar.com/avatar/' + gravatar_hash(email) + '?s=' + size
}

