const API_URL = '/api'

export default class Project {
  constructor(vm) {
    this.vm = vm
    this.resource = vm.$resource('/api/projects/{id}')
  }

  list(done) {
    return this.vm.$http.get('/api/projects?done=' + done);
  }

  create(project) {
    return this.resource.save(project)
  }
}
