const API_URL = '/api'
const SIGNIN_URL = API_URL + '/signin'
const SIGNUP_URL = API_URL + '/signup'

export default {

  user: {
    authenticated: false
  },

  signin(context, creds, redirect) {
    localStorage.setItem('email', creds.email)
    return context.$http.post(SIGNIN_URL, creds).then((resp) => {
      localStorage.setItem('token', resp.data.token)
      if (resp.data.nick) {
        localStorage.setItem('nick', resp.data.nick)
      }
      this.user.authenticated = true
      context.$dispatch('user-signed-in')
      context.$route.router.go(redirect)
    }, (resp) => {
      context.cancelLoading()
      if (resp.status == 422) {
        context.error.email = resp.data.error.email ? resp.data.error.email[0] : null
        context.error.password = resp.data.error.password ? resp.data.error.password[0] : null
      }
      if (resp.status == 429) {
        let seconds = resp.headers('x-retry-after-seconds');
        context.error.email = resp.data.error + ' Retry after ' + seconds + 's.'
      }
      if (resp.status == 401) {
        context.error.email = resp.data.error
      }
    })
  },

  signup(context, creds, redirect) {
    localStorage.setItem('email', creds.email)
    context.$http.post(SIGNUP_URL, creds).then((resp) => {
      localStorage.setItem('token', resp.data.token)
      this.user.authenticated = true
      context.$route.router.go(redirect)
    }, (resp) => {
      context.cancelLoading()
      if (resp.status == 422) {
        context.error.email = resp.data.error.email ? resp.data.error.email[0] : null
        context.error.password = resp.data.error.password ? resp.data.error.password[0] : null
      }
      if (resp.status == 429) {
        let seconds = resp.headers('x-retry-after-seconds');
        context.error.email = resp.data.error + ' Retry after ' + seconds + 's.'
      }
    })
  },

  logout() {
    localStorage.removeItem('token')
    this.user.authenticated = false
  },

  checkAuth() {
    let jwt = localStorage.getItem('token')
    if (jwt) {
      this.user.authenticated = true
      return true
    } else {
      this.user.authenticated = false
      return false
    }
  },


  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    }
  }
}
