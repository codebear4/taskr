<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->string('slug')->nullable();
            $table->string('title');
            $table->text('content')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('status')->default(1);
            $table->integer('visible')->default(1);
            $table->integer('parent_id')->nullable();
            $table->string('parent_type')->nullable();
            $table->timestamps();

            $table->index('slug');
            $table->index('title');
            $table->index('creator_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('task');
    }
}
