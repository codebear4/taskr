<?php

use Illuminate\Database\Seeder;

class Initial extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $creator = factory(App\Models\User::class, 5)->create()->each(function ($u) {
            $project = factory(App\Models\Project::class, 10)->create(['owner_id' => $u->id])->each(function ($p) use ($u) {
                $task = factory(App\Models\Task::class)->create([
                    'creator_id' => $u->id,
                    'project_id' => $p->id,
                ]);
            });
        });
    }
}
