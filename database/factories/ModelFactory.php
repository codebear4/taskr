<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function ($faker) {
    return [
        'name'     => $faker->name,
        'email'    => $faker->safeEmail,
        'password' => password_hash('123456', PASSWORD_BCRYPT),
        'nick'     => $faker->name,
    ];
});

$factory->define(App\Models\Project::class, function ($faker) {
    return [
        'name'      => $faker->name,
        'owner_id'  => $faker->randomDigit,
        'begin_at'  => $faker->dateTimeThisYear(),
        'end_at'    => $faker->dateTimeThisMonth(),
        'is_cycled' => 0,
    ];
});

$factory->define(App\Models\Task::class, function ($faker) {
    return [
        'creator_id' => $faker->randomDigit,
        'slug'       => $faker->slug,
        'title'      => $faker->sentence(),
        'content'    => $faker->paragraph(),
        'project_id' => $faker->randomDigit,
        'status'     => $faker->numberBetween(0, 1),
        'visible'    => $faker->numberBetween(0, 2),
    ];
});
