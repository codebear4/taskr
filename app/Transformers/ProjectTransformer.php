<?php

namespace App\Transformers;

use App\Models\Project;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'parent', 'owner',
    ];

    public function transform(Project $project)
    {
        return [
            'id'       => $project->id,
            'name'       => $project->name,
            'begin_at'   => $project->begin_at,
            'end_at'     => $project->end_at,
            'is_cycled'  => $project->is_cycled,
            'created_at' => $project->created_at,
            'updated_at' => $project->updated_at,
        ];
    }

    public function includeParent(Project $project)
    {
        if ($project->parent === null) {
            return;
        }
        return $this->item($project->parent, new self());
    }

    public function includeOwner(Project $project)
    {
        if ($project->owner === null) {
            return;
        }
        return $this->item($project->owner, new UserTransformer());
    }
}
