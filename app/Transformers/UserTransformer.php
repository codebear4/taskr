<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
    ];

    public function transform(User $user)
    {
        return [
            'name'  => $user->name,
            'email' => $user->email,
        ];
    }
}
