<?php

namespace App\Transformers;

use App\Models\Project;
use App\Models\Task;
use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['parent', 'creator', 'project'];

    public function transform(Task $task)
    {
        return [
            'id'      => $task->id,
            'slug'    => $task->slug,
            'title'   => $task->title,
            'content' => $task->content,
            'slug'    => $task->slug,
            'status'  => $task->status,
            'visible' => $task->visible,
        ];
    }

    public function includeParent(Task $task)
    {
        $parent = $task->parent;

        if ($parent === null) {
            return;
        }

        $className = get_class_name($parent);

        $transformer = sprintf("App\Transformers\%sTransformer", $className);

        return $this->item($parent, new $transformer());
    }

    public function includeCreator(Task $task)
    {
        if ($task->creator === null) {
            return;
        }

        return $this->item($task->creator, new UserTransformer());
    }

    public function includeProject(Task $task)
    {
        if ($task->poject === null) {
            return;
        }

        return $this->item($task->project, new ProjectTransformer());
    }
}
