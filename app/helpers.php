<?php

use Laravel\Lumen\Application;

function get_class_name($instance)
{
    $className = get_class($instance);
    if ($pos = strrpos($className, '\\')) {
        return substr($className, $pos + 1);
    }

    return $pos;
}

function resource(Application $app, string $path, string $controller)
{
    $app->get($path, $controller . '@index');
    $app->get($path . '/{id}', $controller . '@show');
    $app->post($path, $controller . '@store');
    $app->put($path . '/{id}', $controller . '@update');
    $app->delete($path . '/{id}', $controller . '@destroy');
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     *
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}
