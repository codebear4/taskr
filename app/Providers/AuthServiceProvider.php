<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     */
    public function boot()
    {
        app('auth')->viaRequest('api', function ($request) {
            if ($token = $request->bearerToken()) {
                $issuer = new \App\JWT\TokenIssuer();
                $userId = $issuer->parse($token)->getClaim('sub');

                return User::find($userId);
            }
        });
    }
}
