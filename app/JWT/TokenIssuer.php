<?php

namespace App\JWT;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class TokenIssuer
{
    protected $domain = 'https://taskr.xyz';

    public function generate(int $userId) : string
    {
        $issuedAt = time();
        $signer = new Sha256();
        $token = (new Builder)->setIssuer($this->domain)
                                ->setAudience($this->domain)
                                ->setSubject((string) $userId)
                                ->setId(md5(sprintf("%d%d", $userId, $issuedAt)))
                                ->setNotBefore($issuedAt + 60 * 60) // An hour
                                ->setExpiration($issuedAt + 7 * 24* 60 * 60) // A week
                                ->sign($signer, env('JWT_KEY'))
                                ->getToken();
        return (string) $token;
    }

    public function parse(string $token) : Token
    {
        return (new Parser)->parse($token);
    }
}
