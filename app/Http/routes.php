<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['middleware' => 'auth', 'namespace' => 'App\Http\Controllers'], function() use ($app) {
    resource($app, '/api/tasks', 'Frontend\TasksController');
    resource($app, '/api/projects', 'Frontend\ProjectsController');
});

$app->group(['prefix' => '/api', 'namespace' => 'App\Http\Controllers\Frontend'], function() use ($app) {
    $app->post('/signup', 'UsersController@signup');
    $app->post('/signin', 'UsersController@signin');
});
