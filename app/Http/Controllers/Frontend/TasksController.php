<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\ApiController;
use App\Http\Rules\TaskRule;
use App\Models\Task;
use App\Transformers\TaskTransformer;
use Illuminate\Http\Request;

class TasksController extends ApiController
{
    protected $task = null;

    public function index(Request $request)
    {
        $visible = $request->input('visible');
        $status = $request->input('status');
        $query = Task::with('creator', 'parent');
        if ($visible !== null) {
            $query->where('visible', $visible);
        }
        if ($status !== null) {
            $query->where('status', $status);
        }
        $tasks = $query->paginate();

        return $this->respPages($request, $tasks, TaskTransformer::class);
    }

    public function show($id)
    {
        $task = Task::find($id);

        return $this->respOne($task, TaskTransformer::class);
    }

    public function store(Request $request, TaskRule $rule)
    {
        $data = $this->validateRequest($request, $rule);
        $this->setStatusCode(201);

        return $this->respOne(Task::create($data), TaskTransformer::class);
    }

    public function update(int $id, Request $request, TaskRule $rule)
    {
        $data = $this->validateRequest($request, $rule);
        $ret = Task::find($id)->update($data);
        if($ret !== true) {
            $this->setStatusCode(406);
        }
        return $this->respond([]);
    }

    public function destroy(int $id)
    {
        $ret = Task::destroy($id);
        if ($ret === 0) {
            $this->setStatusCode(404);
        }
        return $this->respond([]);
    }
}
