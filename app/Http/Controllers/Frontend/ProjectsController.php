<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\ApiController;
use App\Http\Rules\ProjectRule;
use App\Models\Project;
use App\Transformers\ProjectTransformer;
use Illuminate\Http\Request;

class ProjectsController extends ApiController
{
    protected $project = null;

    public function index(Request $request)
    {
        $done = $request->input('done');
        $query = Project::with('owner', 'parent');
        if ($done !== null) {
            $query->where('done', $done);
        }
        $projects = $query->paginate();

        return $this->respPages($request, $projects, ProjectTransformer::class);
    }

    public function show($id)
    {
        $task = Project::find($id);

        return $this->respOne($task, ProjectTransformer::class);
    }

    public function store(Request $request, ProjectRule $rule)
    {
        $data = $this->validateRequest($request, $rule);
        $this->setStatusCode(201);

        return $this->respOne(Project::create($data), ProjectTransformer::class);
    }

    public function update(int $id, Request $request, ProjectRule $rule)
    {
        $data = $this->validateRequest($request, $rule);
        $ret = Project::find($id)->update($data);
        if($ret !== true) {
            $this->setStatusCode(406);
        }
        return $this->respond([]);
    }

    public function destroy(int $id)
    {
        $ret = Project::destroy($id);
        if ($ret === 0) {
            $this->setStatusCode(404);
        }
        return $this->respond([]);
    }
}
