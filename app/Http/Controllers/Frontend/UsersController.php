<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\ApiController;
use App\Http\Rules\UserLoginRule;
use App\Http\Rules\UserRegisterRule;
use App\JWT\TokenIssuer;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends ApiController
{
    public function signup(Request $request, UserRegisterRule $rule, TokenIssuer $issuer)
    {
        $userData = $this->validateRequest($request, $rule);
        $userData['password'] = password_hash($userData['password'], PASSWORD_BCRYPT);
        $user = User::create($userData);
        $token = $issuer->generate($user->id);

        return response()->json(['token' => $token]);
    }

    public function signin(Request $request, UserLoginRule $rule, TokenIssuer $issuer)
    {
        $userData = $this->validateRequest($request, $rule);
        $user = User::where('email', $userData['email'])->first();
        if ($user === null || !password_verify($userData['password'], $user->password)) {
            return response()->json(['error' => 'Invalid email or password.'], 401);
        }
        $token = $issuer->generate($user->id);

        $data = ['token' => $token];
        if ($user->nick) {
            $data['nick'] = $user->nick;
        }
        return response()->json($data);
    }
}
