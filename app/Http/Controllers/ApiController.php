<?php

namespace App\Http\Controllers;

use App\Http\Rules\Rule;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ApiController extends BaseController
{
    /**
     * Status code for response.
     *
     * @var int
     */
    protected $statusCode = Response::HTTP_OK;
    protected $fractal = null;

    public function __construct(Manager $fractal)
    {
        $this->fractal = $fractal;
        $this->fractal->setSerializer(new ArraySerializer());
    }

    /**
     * Sets the value of statusCode.
     *
     * @param int $statusCode
     *
     * @return ApiController
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Gets the value of statusCode.
     *
     * @return int
     */
    public function getStatusCode() : int
    {
        return $this->statusCode;
    }

    /**
     * Send json reponse.
     *
     * @param array $data
     * @param mixed $headers
     */
    public function respond(array $data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Validate request with rule
     *
     * @param Request $request
     * @param Rule    $rule
     *
     * @return void|array
     */
    public function validateRequest(Request $request, Rule $rule)
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rule->rules(), $rule->messages(), $rule->attributes());

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        } else {
            return $request->only(array_keys($rule->rules()));
        }
    }

    /**
     * Send a single item as json to client
     *
     * @param mixed $item
     * @param string $transformerClass
     * @param array $headers
     */
    public function respOne($item, string $transformerClass, array $headers = [])
    {
        $item = new Item($item, new $transformerClass());
        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
    }

    /**
     * Send number of items to client as json
     *
     * @param array $array
     * @param string $transformerClass
     * @param array $headers
     */
    public function respMany(array $array, string $transformerClass, array $headers = [])
    {
        $array = new Collection($array, new $transformerClass());
        $data = $this->fractal->createData($array)->toArray();

        return $this->respond($data);
    }

    /**
     * Send number of items to clien as json with pagination
     *
     * @param Request $request
     * @param LengthAwarePaginator $paginator
     * @param mixed $transformerClass
     * @param mixed $headers
     */
    public function respPages(Request $request, LengthAwarePaginator $paginator, string $transformerClass, array $headers = [])
    {
        $queryParams = array_diff_key($request->all(), array_flip(['page']));
        $paginator->appends($queryParams);
        $collection = $paginator->getCollection();
        $resource = new Collection($collection, new $transformerClass);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        $data = $this->fractal->createData($resource)->toArray();

        return $this->respond($data);
    }
}

