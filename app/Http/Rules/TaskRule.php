<?php

namespace App\Http\Rules;

class TaskRule extends Rule
{
    public function rules()
    {
        return [
            'slug' => 'sometimes|string',
            'title' => 'required|string',
            'content' => 'sometimes|string',
            'project_id' => 'sometimes|integer|exists:project,id',
            'status' => 'required|integer',
            'visible' => 'sometimes|integer',
            'remind' => 'require|boolean',
            'end_at' => 'sometimes|date_format:"Y-m-d H:i:s"',
        ];
    }
}
