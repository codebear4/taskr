<?php

namespace App\Http\Rules;

abstract class Rule
{
   abstract public function rules();
   public function messages()
   {
       return [];
   }
   public function attributes()
   {
       return [];
   }
}
