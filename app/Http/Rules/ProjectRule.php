<?php

namespace App\Http\Rules;

class ProjectRule extends Rule
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'parent_id' => 'required|integer',
            'is_cycled' => 'required|boolean',
            'end_at' => 'sometimes|date_format:"Y-m-d"',
            'begin_at' => 'sometimes|date_format:"Y-m-d"',
        ];
    }
}

