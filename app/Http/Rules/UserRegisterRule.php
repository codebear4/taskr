<?php

namespace App\Http\Rules;

class UserRegisterRule extends Rule
{
    public function rules()
    {
        return [
            'email' => 'required|email|unique:user,email',
            'password' => 'required|confirmed|string',
        ];
    }
}
