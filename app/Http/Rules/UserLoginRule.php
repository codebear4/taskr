<?php

namespace App\Http\Rules;

class UserLoginRule extends Rule
{
    public function rules()
    {
        return [
            'email' => 'required|email|exists:user,email',
            'password' => 'required|string',
        ];
    }

   public function messages()
   {
       return [
            'email.exists' => 'Your email is not in our system yet.'
       ];
   }

   public function attributes()
   {
       return [];
   }
}
