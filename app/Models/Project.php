<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $dates = ['created_at', 'updated_at', 'end_at', 'begin_at', 'deleted_at'];
    protected $fillable = ['name', 'parent_id', 'owner_id', 'begin_at', 'end_at', 'is_cycled'];

    public function parentable()
    {
        return $this->morphTo();
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }
}
