<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';

    protected $fillable = ['creator_id', 'project_id', 'slug', 'title', 'content', 'status', 'visible'];

    public function parent()
    {
        return $this->morphTo();
    }

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'creator_id');
    }
}
