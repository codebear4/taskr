<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RoutesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'routes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all regitered routes.';

    public function fire()
    {
        $routeCollection = app()->getRoutes();
        $rows = [];
        $x = 0;
        foreach ($routeCollection as $route) {
            if (!empty($route['action']['uses'])) {
                $data = $route['action']['uses'];
                if (($pos = strpos($data, '@')) !== false) {
                    $action = substr($data, $pos + 1);
                }
            } else {
                $action = 'Closure func';
            }
            $rows[$x]['verb'] = $route['method'];
            $rows[$x]['path'] = $route['uri'];
            $rows[$x]['action'] = $action;
            ++$x;
        }
        $headers = ['Verb', 'Path', 'Action'];
        $this->table($headers, $rows);
    }
}
