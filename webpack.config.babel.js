import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import QiniuWebpackPlugin from 'qiniu-webpack-plugin'

let config = {
  entry: {
    app: "./resources/js/app.js",
  },
  output: {
    path: './public/',
    filename: 'js/[name]-[hash].js',
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel!eslint',
      },
      {
        test: /.css$/,
        loader: ExtractTextPlugin.extract('style', 'css')
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract('style', 'css!less')
      },
      {
        test: /\.(woff|woff2)\??.*$/,
        loader: "url?limit=10000&mimetype=application/font-woff&name=/fonts/[name].[ext]"
            },
      {
        test: /\.ttf\??.*$/,
        loader: "file?name=/fonts/[name].[ext]"
            },
      {
        test: /\.eot\??.*$/,
        loader: "file?name=/fonts/[name].[ext]"
            },
      {
        test: /\.svg\??.*$/,
        loader: "file?name=/fonts/[name].[ext]"
            },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
                    'file?hash=sha512&digest=hex&name=images/[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
      {
        test: /.html$/,
        loader: 'html'
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Tasker',
      template: './resources/js/index.ejs',
    }),
    new ExtractTextPlugin('css/[name]-[hash].css', {
      allChunks: true
    }),
    new webpack.ProvidePlugin({
      Vue: 'vue'
    })
  ]
}

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false
    }
  }))
  config.plugins.push(new QiniuWebpackPlugin({
    ACCESS_KEY: process.env.QN_ACCESS_KEY,
    SECRET_KEY: process.env.QN_SECRET_KEY,
    bucket: 'taskr'
  }))
   config.output.publicPath = process.env.QN_URL + '/[hash]/'
}

export default config
