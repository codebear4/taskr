import gulp from 'gulp'
import gutil from 'gulp-util'
import del from 'del'
import webpack from 'webpack'
import webpackConfig from './webpack.config.babel.js'

gulp.task('clean', () => {
  del(['public/css/'])
  del(['public/js/'])
  del(['public/images/'])
  del(['public/fonts/'])
  del(['public/index.html'])
})

gulp.task('webpack', ['clean'], () => {
  webpack(webpackConfig, (err, stats) => {
    gutil.log(stats.toString({
      colors: true
    }))
  })
})

gulp.task('watch', ['clean', 'webpack'], () => {
  gulp.watch(['./resources/js/*.js', './resources/js/*.ejs', './resources/css/*.less', './resources/js/component/*.vue'], ['webpack'])
})

gulp.task('default', ['watch'])
